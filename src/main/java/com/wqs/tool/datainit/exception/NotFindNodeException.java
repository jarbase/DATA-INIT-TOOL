package com.wqs.tool.datainit.exception;


/**
 * 找不到节点异常
 * @author ALEX
 *
 */
public class NotFindNodeException extends Exception{
	
	private static final long serialVersionUID = 1L;
	

	public NotFindNodeException(String name) {
		super("not find node "+name);
	}
	
	public NotFindNodeException(String name,String message) {
		super(name + ":" + message);
	}

}
