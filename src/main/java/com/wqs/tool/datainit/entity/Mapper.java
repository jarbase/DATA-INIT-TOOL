package com.wqs.tool.datainit.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class Mapper implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String name ;
	
	private String type;
	
	private String field;
	
	private String split;
	
}
