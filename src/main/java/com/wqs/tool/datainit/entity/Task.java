package com.wqs.tool.datainit.entity;

import lombok.Data;

@Data
public class Task {
	
	private String name;
	/**
	 * 任务类型   必须
	 * 1.直接设置key和value
	 * 2.设置key，value为从数据库中查找的数据，用gson格式的字符串作为value
	 * 3.设置带动态key
	 */
	private String type;
	/**
	 * 存储类型 hash /set
	 */
	private String storageType;
	/**
	 * type 为1,2时使用
	 */
	private String redisKey;
	/**
	 * type 为3时使用
	 */
	private DynamicKey dynamicKey;
	/**
	 * type 为1时使用
	 */
	private String redisValue;
	/**
	 * type 为2,3时使用
	 */
	private DataBase dataBase;
	/**
	 * 数据保存类型    string , hash, list ,set ,
	 */
	private String dataType;
	
	private String hashKeyField;
	/**关联任务*/
	private String relevanceTasks;
	
}
