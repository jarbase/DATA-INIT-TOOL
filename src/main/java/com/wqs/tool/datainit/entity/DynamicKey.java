package com.wqs.tool.datainit.entity;

import lombok.Data;

/**
 * 动态key
 * @author ALEX
 *
 */
@Data
public class DynamicKey {
	/**
	 * 来源 0 数据库     非必须   默认为0
	 */
	private String source;
	
	/**
	 * 固定的key  非必须 [redisKey与keyField必须有一个存在]     无默认值
	 */
	private String redisKey;
	/**
	 * key值字段    动态的key  非必须 [redisKey与keyField必须有一个存在]  无默认值  
	 * 如果source为0，这keyField对应Mapper中的field
	 */
	private String keyField;
	

}
