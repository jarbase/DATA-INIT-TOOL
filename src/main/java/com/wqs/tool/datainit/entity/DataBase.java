package com.wqs.tool.datainit.entity;

import java.util.List;

import lombok.Data;
/**
 * 数据库连接实体类
 * @author ALEX
 *
 */
@Data
public class DataBase {
	/**数据库驱动名称*/
	private String driverClassName;
	/**数据库连接*/
	private String url;
	/**数据库登录用户名*/
	private String username;
	/**数据库登录密码*/
	private String password;
	/**数据查询语句*/
	private String sqlSentence;
	
	private String className;
	
	private List<Mapper> mappers;
}
