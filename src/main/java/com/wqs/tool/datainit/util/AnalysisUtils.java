package com.wqs.tool.datainit.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.dom4j.Node;

import com.wqs.tool.datainit.common.Constant;
import com.wqs.tool.datainit.common.DataTypeEnum;
import com.wqs.tool.datainit.common.MapCache;
import com.wqs.tool.datainit.entity.DataBase;
import com.wqs.tool.datainit.entity.Mapper;
import com.wqs.tool.datainit.entity.Task;
import com.wqs.tool.datainit.exception.NotFindNodeException;



/**
 * 解析工具
 * @author ALEX
 *
 */
public class AnalysisUtils {
	/**
	 * 解析 type = 1
	 * @param task
	 * @param taskNode
	 * @throws NotFindNodeException 
	 */
	public static void analysisTypeOne(Task task,Node taskNode) throws NotFindNodeException {
		Node redisKeyNode = taskNode.selectSingleNode(Constant.NODE_REDIS_KEY);
 		if(redisKeyNode == null) {
			 throw new NotFindNodeException(Constant.NODE_REDIS_KEY);
		 }else {
			 task.setRedisKey(redisKeyNode.getText());
			 Node redisValueNode = taskNode.selectSingleNode(Constant.NODE_REDIS_VALUE);
		 	if(redisValueNode == null) {
		 		throw new NotFindNodeException(Constant.NODE_REDIS_VALUE);
		 	}else {
		 		task.setRedisValue(redisValueNode.getText());
		 	}
		 }
	}
	
	/**
	 * 解析 type = 2
	 * @param task
	 * @param taskNode
	 * @throws NotFindNodeException
	 */
	public static void analysisTypeTwo(Task task,Node taskNode) throws NotFindNodeException{
		Node redisKeyNode = taskNode.selectSingleNode(Constant.NODE_REDIS_KEY);
		
		if(redisKeyNode == null) {
			 throw new NotFindNodeException(Constant.NODE_REDIS_KEY);
		}else {
			 task.setRedisKey(redisKeyNode.getText());
		}
		
		Node dataType = taskNode.selectSingleNode(Constant.NODE_DATA_TYPE);
		if(dataType == null) {
			task.setDataType(DataTypeEnum.STRING.getType());
		}else {
			task.setDataType(dataType.getText());
		}
		
		
		Node hashKeyFieldNode = taskNode.selectSingleNode(Constant.NODE_HASH_KEY_FIELD);
		if(hashKeyFieldNode!=null) {
			task.setHashKeyField(hashKeyFieldNode.getText());
		}
		
		analysisDatabaseNode(task,taskNode);
		
	}
	
	
	/**
	 * 解析 type = 3
	 * @param task
	 * @param taskNode
	 * @throws NotFindNodeException
	 */
	public static void analysisTypeThree(Task task,Node taskNode) throws NotFindNodeException{
		//<name> 名称
		Node nameNode = taskNode.selectSingleNode(Constant.NODE_NAME);
		if(nameNode == null ) {
			 throw new NotFindNodeException(Constant.NODE_NAME);
		}else {
			task.setName(nameNode.getText());
		}
		//<storageType> 存储类型
		Node storageTypeNode = taskNode.selectSingleNode(Constant.NODE_STORAGE_TYPE);
		if(storageTypeNode == null) {
			 throw new NotFindNodeException(Constant.NODE_STORAGE_TYPE);
		}else {
			 task.setStorageType(storageTypeNode.getText());
		}
		//<redisKey> redis键
		Node redisKeyNode = taskNode.selectSingleNode(Constant.NODE_REDIS_KEY);
		if(redisKeyNode == null) {
			 throw new NotFindNodeException(Constant.NODE_REDIS_KEY);
		}else {
			 task.setRedisKey(redisKeyNode.getText());
		}
		//<relevanceTasks> 关联任务
		Node relevanceTasksNode = taskNode.selectSingleNode(Constant.NODE_RELEVANCE_TASKS);
		if(relevanceTasksNode!=null) {
			task.setRelevanceTasks(relevanceTasksNode.getText());
		}
		
		analysisDatabaseNode(task,taskNode);
		
		cache(task);
		
		
	
	}
	
//	/**
//	 * 解析 type = 4
//	 * 
//	 * @param task
//	 * @param taskNode
//	 * @throws NotFindNodeException
//	 */
//	public static void analysisTypeFour(Task task, Node taskNode) throws NotFindNodeException {
//		// <name> 名称
//		Node nameNode = taskNode.selectSingleNode(Constant.NODE_NAME);
//		if (nameNode == null) {
//			throw new NotFindNodeException(Constant.NODE_NAME);
//		} else {
//			task.setName(nameNode.getText());
//		}
//		// <storageType> 存储类型
//		Node storageTypeNode = taskNode.selectSingleNode(Constant.NODE_STORAGE_TYPE);
//		if (storageTypeNode == null) {
//			throw new NotFindNodeException(Constant.NODE_STORAGE_TYPE);
//		} else {
//			task.setStorageType(storageTypeNode.getText());
//		}
//		
//		//<redisKey> redis键
//		Node redisKeyNode = taskNode.selectSingleNode(Constant.NODE_REDIS_KEY);
//		if(redisKeyNode == null) {
//			 throw new NotFindNodeException(Constant.NODE_REDIS_KEY);
//		}else {
//			 task.setRedisKey(redisKeyNode.getText());
//		}
//		//<relevanceTasks> 关联任务
//		Node relevanceTasksNode = taskNode.selectSingleNode(Constant.NODE_RELEVANCE_TASKS);
//		if(relevanceTasksNode!=null) {
//			task.setRelevanceTasks(relevanceTasksNode.getText());
//		}
//		
//		analysisDatabaseNode(task,taskNode);
//		
//		cache(task);
//		
//		
//	}
	
	
	
	
	
	
	
	/**
	 * 解析<database>节点
	 * @param task
	 * @param taskNode
	 * @throws NotFindNodeException
	 */
	public static void analysisDatabaseNode(Task task,Node taskNode) throws NotFindNodeException {
		Node databaseNode = taskNode.selectSingleNode(Constant.NODE_DATABASE);
		if(databaseNode == null ) {
 			throw new NotFindNodeException(Constant.NODE_DATABASE);
 		}else {
 			Node driverClassNameNode = databaseNode.selectSingleNode(Constant.NODE_DRIVER_CLASS_NAME);
 			Node urlNode = databaseNode.selectSingleNode(Constant.NODE_URL);
 			Node usernameNode = databaseNode.selectSingleNode(Constant.NODE_USERNAME);
 			Node passwordNode = databaseNode.selectSingleNode(Constant.NODE_PASSWORD);
 			Node sqlSentenceNode = databaseNode.selectSingleNode(Constant.NODE_SQL_SENTENCE);
 			Node classNameNode = databaseNode.selectSingleNode(Constant.NODE_CLASS_NAME);
 			Node mappersNode = databaseNode.selectSingleNode(Constant.NODE_MAPPERS);
		
 			if(driverClassNameNode == null || urlNode == null || usernameNode == null 
 					|| passwordNode == null || sqlSentenceNode == null) {
 				throw new NotFindNodeException(Constant.NODE_DATABASE,"Child Node Exception");
 			}else {
 				DataBase database = new DataBase();
 				database.setDriverClassName(driverClassNameNode.getText());
 				database.setUrl(urlNode.getText());
 				database.setUsername(usernameNode.getText());
 				database.setPassword(passwordNode.getText());
 				database.setSqlSentence(sqlSentenceNode.getText());
 				if(classNameNode != null) {
 					database.setClassName(classNameNode.getText());
 				}
 			
 				if(mappersNode!=null) {
	 				List<Node> mapperNodeList = mappersNode.selectNodes(Constant.NODE_MAPPER);
	 				if(mapperNodeList!=null && mapperNodeList.size() > 0) {
	 					List<Mapper> mapperList = new ArrayList<Mapper>();
	 					for (Node mapperNode : mapperNodeList) {
							Mapper mapper = new Mapper();
							Node mapperNameNode = mapperNode.selectSingleNode(Constant.NODE_MAPPER_NAME);
							Node mapperTypeNode = mapperNode.selectSingleNode(Constant.NODE_MAPPER_TYPE);
							Node mapperFieldNode = mapperNode.selectSingleNode(Constant.NODE_MAPPER_FIEDL);
							Node mapperSplitNode = mapperNode.selectSingleNode(Constant.NODE_MAPPER_SPLIT);
							if(mapperNameNode != null) {
								mapper.setName(mapperNameNode.getText());
							}
							if(mapperTypeNode != null) {
								mapper.setType(mapperTypeNode.getText());
							}
							if(mapperFieldNode != null) {
								mapper.setField(mapperFieldNode.getText());
							}
							if(mapperSplitNode != null) {
								mapper.setSplit(mapperSplitNode.getText());
							}
							mapperList.add(mapper);
						}
	 					database.setMappers(mapperList);
	 				}
	 			}
 				task.setDataBase(database);
 			}
 		}
	}
	
	
	public static void cache(Task task) {
		MapCache.taskMap.put(task.getName(), task);
		
		if(task.getRelevanceTasks() != null) {
			String [] array = task.getRelevanceTasks().split(",");
			List<String> list= new ArrayList<>(Arrays.asList(array));
			MapCache.taskRelMap.put(task.getName(), list);
		}
	}
	
	
}
