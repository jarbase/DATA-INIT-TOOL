package com.wqs.tool.datainit.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.beans.BeanMap;

public class DynamicBean {
	//动态生成的类
	private Object object;
	//存放属性的名称和属性的类型
	private BeanMap beanMap;
	
	
	public DynamicBean() {
        super();
    }
	
	public DynamicBean(Map<String,Class> propertyMap,String className) {
        this.object = generateBean(propertyMap,className);
        this.beanMap = BeanMap.create(this.object);
    }

	
	/**
     * @param propertyMap
     * @return
	 * @throws ClassNotFoundException 
     */
    private Object generateBean(Map<String,Class> propertyMap,String className)  {
        BeanGenerator generator = new BeanGenerator();
//        generator.setClassName(className);
        Set<String> keySet = propertyMap.keySet();
        for(Iterator<String> i = keySet.iterator(); i.hasNext(); ) {
            String key = i.next();
            generator.addProperty(key, propertyMap.get(key));
        }
        return generator.create();
    }
    
    /**
               * 给bean属性赋值
     * @param property 属性名
     * @param value 值
     */
    public void setValue(Object property, Object value) {
        beanMap.put(property, value);
    }
    
    /**
                * 通过属性名得到属性值
     * @param property 属性名
     * @return 值
     */
    public Object getValue(String property) {
        return beanMap.get(property);
    }
    
    /**
            * 得到该实体bean对象
     * @return
     */
    public Object getObject() {
        return this.object;
    }
    
}
