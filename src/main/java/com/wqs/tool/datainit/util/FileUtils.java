package com.wqs.tool.datainit.util;

import java.io.File;
import java.io.IOException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class FileUtils {
	
	/**
	 * 从classpath下获取文件
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static File getFileFromClasspath(String fileName) throws IOException {
		Resource resource = new ClassPathResource(fileName);
		return resource.getFile();
	}
	
	
	
}
