package com.wqs.tool.datainit.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yy
 * @date 2019-09-21:10:36
 */
public class StringToListUtils {

    public static List<String> jsonStringToList(String str) {
        String user = str.substring(1, str.length() - 1);
        String[] split = user.split(", ");
        List<String> list = new ArrayList<>();
        for (String value : split) {
            if (value.length() > 2) {
                String s = value.substring(1, value.length() - 1);
                list.add(s);
            }
        }
        return list;
    }
}
