package com.wqs.tool.datainit.redis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wqs.tool.datainit.common.MapCache;
import com.wqs.tool.datainit.entity.Task;
import com.wqs.tool.datainit.util.DataOprationUtils;
import com.wqs.tool.datainit.util.RedisUtils;


@Service
public class RedisReceiver {
	@Autowired
	RedisUtils redis;
	
	 public void receiveMessage(String message) {
	      List<String> list = MapCache.taskRelMap.get(message);
	      if(list != null  && list.size() >0) {
	    	  for (String taskName : list) {
				Task task = MapCache.taskMap.get(taskName);
				
				if(task != null) {
					DataOprationUtils.oprationTypeThree(redis, task);
				}
			  }
	      }
	 }
}
