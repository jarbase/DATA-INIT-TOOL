package com.wqs.tool.datainit.common;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Constant {
	
	
	public final static String REDIS_SUB_TOPIC = "REDIS_DATA_UPDATE";
	
	
	
	
	public final static String XPATH_SEPARATOR="/";
	
	public final static String NODE_TASK = "task";
	
	public final static String NODE_NAME = "name";
	
	public final static String NODE_TYPE = "type";

	public static final String NODE_REDIS_KEY = "redisKey";
	
	public static final String NODE_REDIS_VALUE = "reidsValue";
	
	public static final String NODE_DATA_TYPE = "dataType";
	
	public static final String NODE_HASH_KEY_FIELD = "hashKeyField";
	
	public static final String NODE_DATABASE = "dataBase";
	
	public static final String NODE_DRIVER_CLASS_NAME = "driverClassName";
	
	public static final String NODE_URL = "url";
	
	public static final String NODE_USERNAME = "username";
	
	public static final String NODE_PASSWORD = "password";
	
	public static final String NODE_SQL_SENTENCE = "sqlSentence";
	
	public static final String NODE_CLASS_NAME = "className";
	
	public static final String NODE_MAPPERS = "mappers";
	
	public static final String NODE_MAPPER = "mapper";
	
	public static final String NODE_MAPPER_NAME = "name";
	
	public static final String NODE_MAPPER_TYPE = "type";
	
	public static final String NODE_MAPPER_FIEDL = "field";
	
	public static final String NODE_MAPPER_SPLIT = "split";
	
	public static final String NODE_DYNAMIC_KEY = "dynamicKey";
	
	public static final String NODE_DYNAMIC_KEY_SOURCE = "source";
	
	public static final String NODE_DYNAMIC_KEY_REDIS_KEY = "redisKey";
	
	public static final String NODE_DYNAMIC_KEY_FIELD = "keyField";
	
	public static final String NODE_STORAGE_TYPE = "storageType";
	
	public final static String NODE_RELEVANCE_TASKS = "relevanceTasks";
	
	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		List<String> list = mapper.readValue("[1,2]",new TypeReference<List<String>>() {});
		
		for (String string : list) {
			System.out.println(string);
		}
	}
	
	
}
