package com.wqs.tool.datainit.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wqs.tool.datainit.entity.Task;


public class MapCache {
	/**
	 * 任务map
	 */
	public static Map<String ,Task> taskMap = new HashMap<String ,Task>();
	
	/**
	 * 任务间关系map
	 */
	public static Map<String,List<String>> taskRelMap = new HashMap<String,List<String>>();

}
