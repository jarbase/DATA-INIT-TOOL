package com.wqs.tool.datainit.common;

public enum DataTypeEnum {
	STRING("string"),LIST("list"),HASH("hash"),SET("set");
	
	private String type;
	
	DataTypeEnum(String type){
		this.type = type;
	}

	public String getType() {
		return type;
	}
	
	
}
